# Trabajo Práctico 2

## Parte 2 - API Restful

[Descargar consigna](docs/PracticaAPI_parte_2.pdf)

[Colección de postman para importar](docs/PdS-Practica-1.postman_collection.json)



## Notas
Crear el proyecto

    dotnet new sln -n API
    dotnet new webapi -n API
    dotnet new classlib -n Domain
    dotnet new classlib -n Application
    dotnet new classlib -n AccessData

Agregar proyectos 
    dotnet sln add API/API.csproj 
    dotnet sln add Application/Application.csproj 
    dotnet sln add Domain/Domain.csproj 
    dotnet sln add AccessData/AccessData.csproj 
    
Agregar referencias

    dotnet add API/API.csproj reference Application/Application.csproj
    dotnet add Application/Application.csproj reference Domain/Domain.csproj
    dotnet add AccessData/AccessData.csproj reference Domain/Domain.csproj
    dotnet add AccessData/AccessData.csproj reference Application/Application.csproj

Agregar el paquete para PostgreSQL y otros:

    dotnet add AccessData/AccessData.csproj package Npgsql.EntityFrameworkCore.PostgreSQL
    dotnet add AccessData/AccessData.csproj package Microsoft.EntityFrameworkCore
    dotnet add API/API.csproj package Microsoft.EntityFrameworkCore
    dotnet add API/API.csproj package Microsoft.EntityFrameworkCore.Tools
    dotnet add API/API.csproj package Npgsql.EntityFrameworkCore.PostgreSQL

    dotnet add package SqlKata
    dotnet add package SqlKata.Execution

Crear base de datos

    dotnet new tool-manifest
    dotnet tool install --local dotnet-ef

    dotnet ef migrations add InitialCreate
    dotnet ef database update

Comandos de dotnet-ef:

    dotnet ef migrations add
    dotnet ef migrations list
    dotnet ef migrations script
    dotnet ef dbcontext info
    dotnet ef dbcontext scaffold
    dotnet ef database drop
    dotnet ef database update


Levantar Docker de PostgreSQL

    docker-compose up -d

Borrar todos los contenedores

    docker rm $(docker ps -a -q)

Borrar todas las imágenes

    docker rmi $(docker images -q)