using Domain.Commands;
using Domain.DTOs;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AccessData.Queries;
using Domain.Queries;

namespace Application.Services {

    public interface IProductoServices {
        Producto CreateProducto(ProductoDto producto);
        IEnumerable<Producto> GetProductos();
        List<ProductoDto> GetByCodigo(string codigo);
    }

    public class ProductoServices : IProductoServices {

        private readonly IProductoRepository _repository;
        private readonly IProductoQuery _query;
        
        public ProductoServices(IProductoRepository repository, IProductoQuery query) {
            _repository = repository;
            _query = query;
        }

        public Producto CreateProducto(ProductoDto producto) {
            var entity = new Producto {
                Codigo = producto.Codigo,
                Marca = producto.Marca,
                Nombre = producto.Nombre,
                Precio = producto.Precio,
                Imagen = producto.Imagen
            };

            _repository.Add<Producto>(entity);

            return entity;
        }
        
        public IEnumerable<Producto> GetProductos() {
            return _repository.GetAll<Producto>();
        }

        public List<ProductoDto> GetByCodigo(string codigo) {
           return  _query.GetByCodigo(codigo);
        }

    }   
}
