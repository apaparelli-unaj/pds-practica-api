using Domain.Commands;
using Domain.DTOs;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AccessData.Queries;
using Domain.Queries;

namespace Application.Services {

    public interface IVentaServices {
        ResponseVentasDto AddVenta(VentasDto venta);
        List<ReporteVentasDetailDto> GetVentasDia(string producto);
    }

    public class VentaServices : IVentaServices {

        private readonly IGenericsRepository _repository;
        private readonly IVentaQuery _query;
        
        public VentaServices(IGenericsRepository repository, IVentaQuery query) {
            _repository = repository;
            _query = query;
        }

        public ResponseVentasDto AddVenta(VentasDto venta) {

            var entity_carrito = new Carrito { ClienteId = venta.ClienteId };
            _repository.Add<Carrito>(entity_carrito);

            foreach(int producto in venta.Productos) {
                var entity_carrito_prod = new CarritoProducto {
                    CarritoId = entity_carrito.CarritoId,
                    ProductoId = producto
                };
                _repository.Add<CarritoProducto>(entity_carrito_prod);
            };

            var entity = new Venta {
                CarritoId = entity_carrito.CarritoId,
                Fecha = DateTime.Now
            };
            _repository.Add<Venta>(entity);
            
            return new ResponseVentasDto {
                ClienteId = entity_carrito.ClienteId,
                VentaId = entity.VentaId,
                Fecha = entity.Fecha
            };

        }
        
        public List<ReporteVentasDetailDto> GetVentasDia(string producto) {
           return  _query.GetVentasDia(producto);
        }

    }   
}
