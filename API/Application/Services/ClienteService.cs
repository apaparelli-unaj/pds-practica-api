using Domain.Commands;
using Domain.DTOs;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Domain.Queries;


namespace Application.Services {

    public interface IClienteServices {
        Cliente CreateCliente(ClienteDto cliente);
        IEnumerable<Cliente> GetClientes();
        List<ClienteDto> SearchCliente(string q);
    }

    public class ClienteServices : IClienteServices {

        private readonly IGenericsRepository _repository;
        private readonly IClienteQuery _query;

        public ClienteServices(IGenericsRepository repository, IClienteQuery query) {
            _repository = repository;
            _query = query;
            
        }

        public Cliente CreateCliente(ClienteDto cliente) {
            var entity = new Cliente {
                DNI = cliente.DNI,
                Nombre = cliente.Nombre,
                Apellido = cliente.Apellido,
                Direccion = cliente.Direccion,
                Telefono = cliente.Telefono
            };

            _repository.Add<Cliente>(entity);

            return entity;
        }
        
        public IEnumerable<Cliente> GetClientes(){
            return _repository.GetAll<Cliente>();
        }
        
        public List<ClienteDto> SearchCliente(string q) {
           return  _query.SearchCliente(q);
        }
    }
}
