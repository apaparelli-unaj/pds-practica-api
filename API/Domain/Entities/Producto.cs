using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Entities
{
    public class Producto {

        public int ProductoId { get; set; }
        [StringLength(45)]
        public string Codigo { get; set; }
        [StringLength(45)]
        public string Marca { get; set; }
        [StringLength(45)]
        public string Nombre { get; set; }
        [Column(TypeName = "decimal(15,2)")]
        public decimal Precio { get; set; }
        [StringLength(128)]
        public string Imagen { get; set; }

        public ICollection<CarritoProducto> CarritoProductos { get; set; }
    }
}