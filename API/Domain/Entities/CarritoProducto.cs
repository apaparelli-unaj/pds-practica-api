using System;
using System.Collections.Generic;


namespace Domain.Entities
{
    public class CarritoProducto {

        public int CarritoId { get; set; }
        public int ProductoId { get; set; }

        public Carrito Carritos { get; set; }
        public Producto Productos { get; set; }
    }
}