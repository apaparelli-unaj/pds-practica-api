using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Domain.Entities
{
    public class Cliente {

        public int ClienteId { get; set; }
        [StringLength(10)]
        public string DNI { get; set; }
        [StringLength(45)]
        public string Nombre { get; set; }
        [StringLength(45)]
        public string Apellido { get; set; }
        [StringLength(45)]
        public string Direccion { get; set; }
        [StringLength(45)]
        public string Telefono { get; set; }

        public ICollection<Carrito> Carritos { get; set; }

    }
}