using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Domain.Entities
{
    public class Carrito {

        public int CarritoId { get; set; }
        public int ClienteId { get; set; }

        public Cliente Clientes { get; set; }
        public ICollection<Venta> Ventas { get; set; }
        public ICollection<CarritoProducto> CarritoProductos { get; set; }
    }
}