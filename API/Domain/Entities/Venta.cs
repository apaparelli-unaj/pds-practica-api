using System;
using System.Collections.Generic;


namespace Domain.Entities
{
    public class Venta {

        public int VentaId { get; set; }
        public int CarritoId { get; set; } 
        public DateTime Fecha { get; set; }
        
        public Carrito Carritos { get; set; }
    }
}