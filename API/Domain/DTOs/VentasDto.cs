using System;
using System.Collections.Generic;


namespace Domain.DTOs {

    public class VentasDto {
        public int ClienteId { get; set; }
        public List<int> Productos { get; set; }
    }
    
    public class ResponseVentasDto {
        public int VentaId { get; set; }
        public int ClienteId { get; set; }
        public DateTime Fecha { get; set; }
    }

    public class ReporteVentasDetailDto {
        public int VentaId { get; set; }
        public DateTime Fecha { get; set; }
        public string NombreCliente { get; set; }
        public string ApellidoCliente { get; set; }
        public string DNICliente { get; set; }
        public string CodigoProducto { get; set; }
        public string MarcaProducto { get; set; }
        public string NombreProducto { get; set; }
        public decimal Precio { get; set; }
    }

}
 