

namespace Domain.DTOs {

    public class ProductoDto {
        public int ProductoId { get; set; }
        public string Codigo { get; set; }
        public string Marca { get; set; }
        public string Nombre { get; set; }
        public decimal Precio { get; set; }
        public string Imagen { get; set; }

    }
}
 