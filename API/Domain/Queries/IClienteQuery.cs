using System.Collections.Generic;
using Domain.DTOs;

namespace Domain.Queries {

    public interface IClienteQuery {
        List<ClienteDto> SearchCliente(string q); 
    }
}