using System.Collections.Generic;
using Domain.DTOs;

namespace Domain.Queries {

    public interface IVentaQuery {
        List<ReporteVentasDetailDto> GetVentasDia(string producto); 
    }
}