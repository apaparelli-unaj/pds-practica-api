using System.Collections.Generic;
using Domain.DTOs;

namespace Domain.Queries {

    public interface IProductoQuery {
        List<ProductoDto> GetByCodigo(string codigo); 
    }
}