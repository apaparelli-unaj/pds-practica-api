using System.Threading.Tasks;
using Domain.Entities;


namespace Domain.Commands {
    public interface IProductoRepository: IGenericsRepository {
        Task<Producto> GetByCodigo(string firstName);
    }
}
