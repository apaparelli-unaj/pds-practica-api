﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class insertDummyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.InsertData(
                table: "Productos",
                columns: new[] {"Codigo", "Marca", "Nombre", "Precio", "Imagen"},
                values: new object[,] {
                    {"0001", "Quilmes", "Cervesa", 89.99, "cervesa-quilmes.jpg"},
                    {"0002", "Stella", "Cervesa", 120.00, "cervesa-stella.jpg"},
                    {"0003", "Sancor", "Crema", 110, "crema-sancor.jpg"},
                    {"0004", "Matarazzo", "Fideos largos", 45, "fideos-largos.jpg"},
                    {"0005", "Matarazzo", "Fideos Tirabuzon", 50, "fideos-tirabuzon.jpg"},
                    {"0006", "Las Tres Niñas", "Leche", 70, "leche-lastresninas.jpg"},
                    {"0007", "Sancor", "Leche", 60.50, "leche-sancor.jpg"},
                    {"0008", "Amanda", "Yerba", 130.55, "yerba-amanda.jpg"},
                    {"0009", "Taragui", "Yerba", 140, "yerba-taragui.jpg"}
                });

            migrationBuilder.InsertData(
                table: "Clientes",
                columns: new[] {"DNI", "Nombre", "Apellido", "Direccion", "Telefono"},
                values: new object[,] {
                    {"123456789", "John", "Doe", "Av. Calchaqui 1234", "4444-4444"},
                    {"987654321", "Juan", "Perez", "Av. San Martin 4321", "8888-8888"},
                    {"30301301", "Alberto", "Paparelli", "Sallares 222", "1234-5678"}
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
