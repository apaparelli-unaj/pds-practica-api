using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Application.Services;
using Domain.DTOs;
using Domain.Entities;


namespace API.Controllers {

    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase {
        private readonly IClienteServices _service;

        public ClienteController(IClienteServices service) {
            _service = service;
        }

        [HttpPost]
        public ActionResult<Cliente> Post(ClienteDto cliente) {
            return StatusCode(201, _service.CreateCliente(cliente));
        }

        [HttpGet]
        public IActionResult GetCursos([FromQuery] string q) {
            try {
                return new JsonResult(_service.SearchCliente(q)) { StatusCode = 200 };
            }
            catch (Exception e) {
                return BadRequest(e.Message);
            }
        }
    }
}