using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Application.Services;
using Domain.DTOs;
using Domain.Entities;


namespace API.Controllers {

    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase {
        private readonly IProductoServices _service;

        public ProductoController(IProductoServices service) {
            _service = service;
        }

        [HttpPost]
        public ActionResult<Producto> Post(ProductoDto cliente) {
            return StatusCode(201, _service.CreateProducto(cliente));
        }

        [HttpGet]
        public IActionResult GetCursos([FromQuery] string codigo) {
            try {
                return new JsonResult(_service.GetByCodigo(codigo)) { StatusCode = 200 };
            }
            catch (Exception e) {
                return BadRequest(e.Message);
            }
        }

    }
}