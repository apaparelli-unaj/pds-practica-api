using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Application.Services;
using Domain.DTOs;
using Domain.Entities;


namespace API.Controllers {

    [Route("api/[controller]")]
    [ApiController]
    public class VentaController : ControllerBase {
        private readonly IVentaServices _service;

        public VentaController(IVentaServices service) {
            _service = service;
        }

        [HttpPost]
        public IActionResult AddVenta(VentasDto venta) {
            try {
                return new JsonResult(_service.AddVenta(venta)) { StatusCode = 201 };
            }
            catch (Exception e) {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public IActionResult GetVentasDia([FromQuery] string producto) {
            try {
                return new JsonResult(_service.GetVentasDia(producto)) { StatusCode = 200 };
            }
            catch (Exception e) {
                return BadRequest(e.Message);
            }
        }

    }
}