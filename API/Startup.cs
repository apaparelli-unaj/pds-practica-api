using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using AccessData;
using AccessData.Queries;
using AccessData.Commands;
using Application.Services;
using Domain.Commands;
using Domain.Queries;
using System.Data.SqlClient; 
using System.Data;
using SqlKata.Compilers;


namespace API {

    public class Startup {
        // readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {

            services.AddCors(c =>  {  
                c.AddPolicy("AllowOrigin", options => options
                                                            .AllowAnyOrigin()
                                                            .AllowAnyMethod()
                                                            .AllowAnyHeader());
            }); 

            services.AddControllers();
            var connectionString = Configuration.GetSection("ConnectionString").Value;
            services.AddDbContext<APIDbContext>(options => options.UseNpgsql(connectionString));

            services.AddTransient<Compiler, PostgresCompiler>();
            services.AddTransient<IDbConnection>(b => {
                return new  Npgsql.NpgsqlConnection(connectionString);
            });

            services.AddTransient<IGenericsRepository, GenericsRepository>();
            services.AddTransient<IProductoRepository, ProductoRepository>();
            services.AddTransient<IClienteServices, ClienteServices>();
            services.AddTransient<IProductoServices, ProductoServices>();
            services.AddTransient<IVentaServices, VentaServices>();
            services.AddTransient<IProductoQuery, ProductoQuery>();
            services.AddTransient<IClienteQuery, ClienteQuery>();
            services.AddTransient<IVentaQuery, VentaQuery>();

            services.AddControllers();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {

            app.UseCors(options => options.AllowAnyOrigin()
                                          .AllowAnyHeader()
                                          .AllowAnyHeader()); 

            if (env.IsDevelopment()){
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();

            app.UseRouting();

            // app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
                        

        }
    }
}
