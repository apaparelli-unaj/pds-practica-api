using Microsoft.EntityFrameworkCore;
using Domain.Entities;

namespace AccessData {
    public class APIDbContext : DbContext{
        
        public APIDbContext(DbContextOptions<APIDbContext> options): base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<CarritoProducto>().HasKey(sc => new { sc.CarritoId, sc.ProductoId });
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Carrito> Carritos { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<CarritoProducto> CarritoProductos { get; set; }
        public DbSet<Venta> Ventas { get; set; }

    }
}
