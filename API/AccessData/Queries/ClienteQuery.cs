
using SqlKata.Compilers;
using SqlKata.Execution;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Domain.DTOs;
using Domain.Queries;


namespace AccessData.Queries {

    public class ClienteQuery : IClienteQuery {

        private readonly IDbConnection connection;
        private readonly Compiler sqlKataCompiler;

        public ClienteQuery(IDbConnection connection, Compiler sqlKataCompiler) {
            this.connection = connection;
            this.sqlKataCompiler = sqlKataCompiler;
        }

        public List<ClienteDto> SearchCliente(string search_q) {
            var db = new QueryFactory(connection, sqlKataCompiler);

            var query = db.Query("Clientes")
                .Select("ClienteId",
                        "DNI",
                        "Nombre",
                        "Apellido",
                        "Direccion",
                        "Telefono")
                .When(!string.IsNullOrWhiteSpace(search_q), q => q
                    .WhereLike("DNI", $"%{search_q}%")
                    .OrWhereLike("Nombre", $"%{search_q}%")
                    .OrWhereLike("Apellido", $"%{search_q}%")
                    );

            var result = query.Get<ClienteDto>();

            return result.ToList();
        }

    }
}
