
using SqlKata.Compilers;
using SqlKata.Execution;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Domain.DTOs;
using Domain.Queries;


namespace AccessData.Queries {

    public class VentaQuery : IVentaQuery {

        private readonly IDbConnection connection;
        private readonly Compiler sqlKataCompiler;

        public VentaQuery(IDbConnection connection, Compiler sqlKataCompiler) {
            this.connection = connection;
            this.sqlKataCompiler = sqlKataCompiler;
        }

        public List<ReporteVentasDetailDto> GetVentasDia(string producto) {

            var db = new QueryFactory(connection, sqlKataCompiler);

            var query = db.Query("Ventas")
                .Select("VentaId",
                        "Clientes.Nombre as NombreCliente",
                        "Clientes.Apellido as ApellidoCliente",
                        "Clientes.DNI as DNICliente",
                        "Productos.Codigo as CodigoProducto",
                        "Productos.Marca as MarcaProducto",
                        "Productos.Nombre as NombreProducto",
                        "Productos.Precio")
                .Join("Carritos", "Carritos.CarritoId", "Ventas.CarritoId")
                .Join("Clientes", "Carritos.ClienteId", "Clientes.ClienteId")
                .Join("CarritoProductos", "CarritoProductos.CarritoId", "Carritos.CarritoId")
                .Join("Productos", "CarritoProductos.ProductoId", "Productos.ProductoId")
                .When(!string.IsNullOrWhiteSpace(producto), q => q
                    .WhereLike("Productos.Marca", $"%{producto}%")
                    .OrWhereLike("Productos.Nombre", $"%{producto}%")
                    .OrWhereLike("Productos.Codigo", $"%{producto}%"));


            var result = query.Get<ReporteVentasDetailDto>();

            return result.ToList();

            // return new ResponseGetCursoById {
            //     CursoId = curso.CursoId,
            //     Materia = curso.Materia,
            //     Profesor = profesor,
            //     Alumnos = alumnos
            // };


            // var db = new QueryFactory(connection, sqlKataCompiler);
            // var curso = db.Query("Ventas")
            //     .Select("CursoId", "Materia", "ProfesorId")
            //     .Where("CursoId", "=", cursoId)
            //     .FirstOrDefault<ResponseGetAllCursoDto>();

            // var profesor = db.Query("Profesores")
            //     .Select("ProfesorId", "Nombre", "Apellido")
            //     .Where("ProfesorId", "=", curso.ProfesorId)
            //     .FirstOrDefault<ResponseGetCursoByIdProfesor>();

            // var ventas = db.Query("Ventas")
            //     .Select("VentaID", "Fecha")
            //     .Where("Fecha", "=", DateTime.Today)
            //     .Get<ResponseGetCursoByIdAlumno>()
            //     .ToList();

            // return new ResponseGetCursoById
            // {
            //     CursoId = curso.CursoId,
            //     Materia = curso.Materia,
            //     Profesor = profesor,
            //     Alumnos = alumnos
            // };

        }

    }
}
