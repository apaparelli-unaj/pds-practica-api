
using SqlKata.Compilers;
using SqlKata.Execution;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Domain.DTOs;
using Domain.Queries;


namespace AccessData.Queries {

    public class ProductoQuery : IProductoQuery {

        private readonly IDbConnection connection;
        private readonly Compiler sqlKataCompiler;

        public ProductoQuery(IDbConnection connection, Compiler sqlKataCompiler) {
            this.connection = connection;
            this.sqlKataCompiler = sqlKataCompiler;
        }

        public List<ProductoDto> GetByCodigo(string codigo) {

            var db = new QueryFactory(connection, sqlKataCompiler);

            var query = db.Query("Productos")
                .Select("ProductoId",
                        "Codigo",
                        "Marca",
                        "Nombre",
                        "Precio",
                        "Imagen")
                .When(!string.IsNullOrWhiteSpace(codigo), q => q.WhereLike("Codigo", $"%{codigo}%"));

            var result = query.Get<ProductoDto>();

            return result.ToList();
        }

    }
}
