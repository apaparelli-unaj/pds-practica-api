using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Domain.Commands;
using Domain.Entities;


namespace AccessData.Commands {

    public class ProductoRepository : GenericsRepository, IProductoRepository {
        private readonly APIDbContext _context;

        public ProductoRepository(APIDbContext dbContext) : base(dbContext) { 
            _context = dbContext;
        }

        public Task<Producto> GetByCodigo(string codigo) {
            return _context.Set<Producto>().FirstOrDefaultAsync(p => p.Codigo == codigo);
        }

    }
}
